const Hapi = require('@hapi/hapi');
const Nes = require('@hapi/nes');
import {r} from './routes/index';
import { connectDB } from './utils/db';
import { EventListener} from '../server/jobs/index';
import config from './config/config';

export const server = new Hapi.Server({
    port: config.server.port,
    host: config.server.host,
});

const init = async function() {

    await server.register(Nes);
    server.subscription('/bridge/');
    server.route(r);

    await server.start();
    console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});

export async function startServer() {
    if (await connectDB()) {
        await init();
        await EventListener();
    }
}