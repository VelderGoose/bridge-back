

export function output(res?: object | null): object {
    return {
      ok: true,
      result: res,
    };
  }