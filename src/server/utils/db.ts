import { sequelize } from "../models/index";
import { Block } from "../models/block";
import { Transaction } from "../models/transaction";

export async function connectDB() {
    try {
        await sequelize.authenticate();
        await sequelize.sync();
        console.log('Connection has been established successfully.');
        return true;
    } catch (error) {
        console.error('Unable to connect to the database:', error);
        return false;
    }
}