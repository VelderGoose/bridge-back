import { Transaction } from "../models/transaction";
import { Op } from "sequelize/types";
import config from '../config/config';
import { output } from "../utils";
const Web3 = require('web3');

export const showTransactions = async function(r){
    let txList = Transaction.findAll({where:{ [Op.or]: [
        {sender: r.payload.userAddress},
        {recipient: r.payload.userAddress},
    ]}});
    return output(txList);
}

export const getKey = async function(r, h){
    const userAdsress = await Web3.eth.accounts.recover({
        messageHash: r.payload.signature.messageHash,
        v: r.payload.signature.v,
        r: r.payload.signature.r,
        s: r.payload.signature.s,
    });
    let tx = await Transaction.findOne({where:{ txHash: r.payload.transactionHash, recipient: userAdsress,
                                        status: "created"}});
    if(tx){
        const messege = await Web3.utils.soliditySha3(
            tx.amount,
            tx.nonce,
            tx.sender,
            tx.recipient,
            tx.tokenSymbol,
            tx.chainFrom,
            tx.chainTo
        );
        const signature = await Web3.eth.accounts.sign(messege, config.broker);
        return output(signature);
    }
    else{
        return h.response(`Transaction ${r.payload.transactionHash} not found`).code(404);
    }
}