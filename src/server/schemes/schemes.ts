const Joi = require('joi');

export const paramsSchema = Joi.object({
});

export const payloadSchema = Joi.object({
    signature: Joi.object({
        message: Joi.string(),
        messageHash: Joi.string(),
        v: Joi.string(),
        r: Joi.string(),
        s: Joi.string(),
        signature: Joi.string(),
    })
});

export const querySchema = Joi.object({
    hash: Joi.string(),
});