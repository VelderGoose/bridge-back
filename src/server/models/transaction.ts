import { Sequelize, Column, ForeignKey, DataType, Table, Model, PrimaryKey, BelongsTo, HasMany, AutoIncrement, Default, AllowNull } from 'sequelize-typescript';
import { User } from './user';

@Table
export class Transaction extends Model{
    @PrimaryKey
    @Column({
        type: DataType.STRING,
        allowNull: false
    })
    txHash: string
    @Column({
        type: DataType.STRING,
        allowNull: false
    })
    tokenSymbol: string
    @Column({
        type: DataType.DECIMAL,
        allowNull: false
    })
    nonce: number
    @Column({
        type: DataType.DECIMAL,
        allowNull: false
    })
    chainFrom: number
    @Column({
        type: DataType.STRING,
        allowNull: false
    })
    sender: string
    @Column({
        type: DataType.DECIMAL,
        allowNull: false
    })
    chainTo: number
    @Column({
        type: DataType.STRING,
        allowNull: false
    })
    recipient: string
    @Column({
        type: DataType.DECIMAL,
        allowNull: true
    })
    amount: number
    @Column({
        type: DataType.DECIMAL,
        allowNull: false
    })
    time: number
    @Column({
        type: DataType.STRING,
        allowNull: false
    })
    txType: string
    @Column({
        type: DataType.STRING,
        allowNull: false
    })
    status: string
}