import { Transaction } from './transaction';
import { Sequelize, Column, ForeignKey, DataType, Table, Model, PrimaryKey, BelongsTo, HasMany, AutoIncrement, Default, AllowNull } from 'sequelize-typescript';

@Table
export class User extends Model{
    @PrimaryKey
    @Column({
        type: DataType.UUID,
        defaultValue: DataType.UUIDV4,
        allowNull: false
    })
    id: string
    @Column({
        type: DataType.STRING,
        allowNull: false
    })
    address: string
    /*
    @HasMany(()=>Transaction)
    transactions: Transaction[]
    */
}
