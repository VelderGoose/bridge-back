import { Sequelize, Column, ForeignKey, DataType, Table, Model, PrimaryKey, AutoIncrement} from 'sequelize-typescript';

@Table
export class Block extends Model{
    @Column({
        type: DataType.STRING,
        allowNull: false
    })
    chainName: string
    @Column({
        type: DataType.INTEGER,
        allowNull: true
    })
    blockNumber: number
}