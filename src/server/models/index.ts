import { Sequelize, Column, ForeignKey, DataType, Table, Model, PrimaryKey, BelongsTo, HasMany, AutoIncrement, Default } from 'sequelize-typescript';
import { DATE } from "sequelize/types";
import { Transaction } from './transaction';
import { User } from './user';
import { Block } from './block';

export const sequelize = new Sequelize('postgres://postgres:zsxadawsqe@localhost:5432/proj', {
    dialect: 'postgres',
    models: [Transaction, User, Block],
});