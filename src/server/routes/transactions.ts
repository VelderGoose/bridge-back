import * as Joi from 'joi';
import * as Schemes from "../schemes/schemes";
import * as Handlers from '../api/api';

export const routes: any[] = [
    {
        method: 'POST',
        path: '/gettransactions',
        handler: Handlers.showTransactions
    },
    {
        method: 'POST',
        path: '/getkey',
        options:{
            validate:{
                query: Schemes.querySchema,
                payload: Schemes.payloadSchema
            }
        },
        handler: Handlers.getKey
    },
]