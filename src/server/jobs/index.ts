import { Block } from './../models/block';
import { Transaction } from './../models/transaction';
const Web3 = require('web3');
import config from '../config/config';
const ethers = require('ethers');
import { server, /*userSocket*/} from './../index';
import { Op } from "sequelize/types";

async function createTx(data, chainFrom, chainTo, type){
    let tx = await Transaction.create({txHash: data.transactionHash, tokenSymbol: data.returnValues.tokenSymbol,
        nonce: data.returnValues.nonce, chainFrom: chainFrom, sender: data.returnValues.sender, chainTo: chainTo,
        recipient: data.returnValues.recepient, amount: data.returnValues.amount, time: data.returnValues.time,
        txType: type, status: "created"});
    await server.publish("/bridge/", tx);
}

async function eventSwapHandler(event){
    await createTx(event, config.testChainId.ethRinkeby, config.testChainId.bscTestnet, event.event);
}

async function eventRedeemHandler(event){
    //swapHash - это прикидка, контракт пока ничего на этот счет не ответил
    await Transaction.update({status: "completed"}, {where:{ sender: event.returnValues.sender,
                            recipient: event.returnValues.recepient, txHash: event.returnValues.swapHash}});
    await server.publish("/bridge/", "The transaction was successful");
}

export async function EventListener(arg = null){
    try{
        if(arg == null || arg == 1){
            const web3Eth = new Web3(
                new Web3.providers.WebsocketProvider(config.provider.ethereum)
            );
            const ethContract = new web3Eth.eth.Contract(config.abi, config.contract.ethereum);
            let [blockEth, findBlockEth] = await Block.findCreateFind({where:{chainName: "Ethereum"}, defaults:{chainName: "Ethereum"}});

            const listenEth = ethContract.events.allEvents({
                fromBlock: blockEth.blockNumber,
            }, async function(error, result){
                if(!error){
                    let tx = await Transaction.findByPk(result.transactionHash);
                    if(!tx){
                        console.log(result);
                        switch(result.event){
                            case 'eventSwap':
                                await eventSwapHandler(result);
                                break;
                            case 'eventRedeem':
                                await eventRedeemHandler(result);
                                break;
                        }
                    }
                }
                else{
                    console.log(result.event, "ERROR");
                    return;
                }
            });

            const subscriptionsEth = web3Eth.eth.subscribe('newBlockHeaders', async function(error, result){
                if(!error){
                    await blockEth.update({blockNumber: result.number});
                }
                else{
                    console.log(error, "SUBSCRIPTION ERROR");
                    subscriptionsEth.unsubscribe(function(error, success){
                        if(success){
                            console.log('ETH successfully unsubscribed!');
                        }
                        return setTimeout(EventListener(1), 1000);
                    });
                }
            });
        }

        if(arg == null || arg == 2){
            const web3Bsc = new Web3(
                new Web3.providers.WebsocketProvider(config.provider.binance)
            );
            const bscContract = new web3Bsc.eth.Contract(config.abi, config.contract.binance);
            let [blockBsc, findBlockBsc] = await Block.findCreateFind({where:{chainName: "Binance Smart Chain"}, defaults:{chainName: "Binance Smart Chain"}});

            const listenBsc = bscContract.events.allEvents({
                fromBlock: blockBsc.blockNumber,
            }, async function(error, result){
                if(!error){
                    let tx = await Transaction.findByPk(result.transactionHash);
                    if(!tx){
                        console.log(result);
                        switch(result.event){
                            case 'eventSwap':
                                await eventSwapHandler(result);
                                break;
                            case 'eventRedeem':
                                await eventRedeemHandler(result);
                                break;
                        }
                    }
                }
                else{
                    console.log(result.event, "ERROR");
                    return;
                }
            });

            const subscriptionsBsc = web3Bsc.eth.subscribe('newBlockHeaders', async function(error, result){
                if(!error){
                    await blockBsc.update({blockNumber: result.number});
                }
                else{
                    console.log(error, "SUBSCRIPTION ERROR");
                    subscriptionsBsc.unsubscribe(function(error, success){
                        if(success){
                            console.log('BSC successfully unsubscribed!');
                        }
                        return setTimeout(EventListener(2), 1000);
                    });
                }
            });
        }
    } catch(err){
        console.log(err);
        return setTimeout(EventListener(), 1000);
    }
}