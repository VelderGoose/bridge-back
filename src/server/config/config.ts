import { bridge } from './../lib/index';
import { config, } from 'dotenv';

config();

export default  {
    dbLink: process.env.DB_LINK,
    server: {
        port: process.env.SERVER_PORT ? Number(process.env.SERVER_PORT) : 3000,
        host: process.env.SERVER_HOST ? process.env.SERVER_HOST : 'localhost',
    },
    broker: process.env.PRIVATE_KEY,
    provider: {
        ethereum: process.env.ETH_PROVIDER,
        binance: process.env.BSC_PROVIDER,
    },
    contract: {
        ethereum: process.env.ETH_CONTRACT_ADRESS,
        binance: process.env.BSC_CONTRACT_ADRESS,
    },
    testChainId:{
        ethRinkeby: process.env.ETH_RINKEBY_ID,
        bscTestnet: process.env.BSC_TESTNET_ID,
    },
    abi: bridge,
}