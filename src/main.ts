import * as main from './server';

try {
  main.startServer();
}
catch (e) {
  console.error(e);
}